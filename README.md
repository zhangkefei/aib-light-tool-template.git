# aib-light-tool-template

## 介绍
这是AIB“轻”工具模板。开发者基于这个模板，能够快速开发出可以在与阿布对话中使用的“轻”工具。

## 使用方法

1. 克隆此仓库，[仓库地址](https://gitee.com/zhangkefei/aib-light-tool-template.git)。
2. 安装依赖：cd aib-light-tool-template && npm i
3. 运行开发环境：npm run dev

> 开发环境中，提示词会自动触发。仓库是一个从文本生成md5的“轻”工具示例

## 开发步骤

1. 修改package.json
2. 修改README.md
3. 在lib/main.js中写入逻辑，最终需要导出一个函数
4. 在开发环境中测试无误后，运行npm run build构建生产版本
5. 可以使用npm run staging命令进行发布前测试
6. 最后使用bitones.cn官网->开发者->发布应用，选择构建后的dist目录，发布应用。