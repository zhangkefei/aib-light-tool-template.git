importScripts('/lodash.min.js')
let confirmResolve, alertResolve, fileResolve, dirHandleResolve, promptResolve, processingResolve, completeResolve, platformResolve
self.$ = {
    message(msg) {
        postMessage({
            command: 'message',
            info: {
                message: msg
            }
        })
    },
    warning(msg) {
        postMessage({
            command: 'warning',
            info: {
                message: msg
            }
        })
    },
    error(msg) {
        postMessage({
            command: 'error',
            info: {
                message: msg
            }
        })
    },
    processing() {
        postMessage({
            command: 'processing'
        })
        return new Promise(res => {
            processingResolve = res
        })
    },
    complete(p) {
        postMessage({
            command: 'complete',
            info: {
                message: p
            }
        })
        return new Promise(res => {
            completeResolve = res
        })
    },
    alert(msg) {
        postMessage({
            command: 'alert',
            info: {
                message: msg
            }
        })
        return new Promise((res) => {
            alertResolve = res
        })
    },
    prompt(msg) {
        postMessage({
            command: 'prompt',
            info: {
                message: msg
            }
        })
        return new Promise((res) => {
            promptResolve = res
        })
    },
    confirm(msg) {
        postMessage({
            command: 'confirm',
            info: {
                message: msg
            }
        })
        return new Promise((res) => {
            confirmResolve = res
        })
    },
    file(msg) {
        postMessage({
            command: 'file',
            info: {
                message: msg
            }
        })
        return new Promise((res) => {
            fileResolve = res
        })
    },
    dirHandle(msg) {
        postMessage({
            command: 'dir-handle',
            info: {
                message: msg
            }
        })
        return new Promise((res) => {
            dirHandleResolve = res
        })
    },
    platform() {
        postMessage({
            command: 'platform'
        })
        return new Promise(res => {
            platformResolve = res
        })
    },
    result(data) {
        postMessage({
            command: 'result',
            info: {
                data
            }
        })
    }
}

async function execute(info) {
    const { params } = info
    const url = `/dist/main.js`
    importScripts(url)
    if (self.main) {
        const result = await self.main(...params)
        if (![null, undefined].includes(result)) {
            $.result(result)
        }
    }
    postMessage({
        command: 'finish'
    })
}

self.onmessage = async ({ data }) => {
    console.log(data);
    const { command, info } = data
    switch (command) {
        case 'execute':
            await execute(info)
            break;
        case 'processing-response':
            const { response: processingResult } = info
            processingResolve && processingResolve(processingResult)
            processingResolve = undefined
            break;
        case 'complete-response':
            completeResolve && completeResolve()
            completeResolve = undefined
            break;
        case 'alert-response':
            alertResolve && alertResolve()
            alertResolve = undefined
            break;
        case 'confirm-response':
            const { response: confirmResult } = info
            confirmResolve && confirmResolve(confirmResult)
            alertResolve = undefined
            break;
        case 'prompt-response':
            const { response: promptResult } = info
            promptResolve && promptResolve(promptResult)
            promptResolve = undefined
            break;
        case 'file-response':
            const { response: fileResult } = info
            fileResolve && fileResolve(fileResult)
            fileResolve = undefined
            break;
        case 'dir-handle-response':
            const { response: dirHandleResult } = info
            dirHandleResolve && dirHandleResolve(dirHandleResult)
            dirHandleResolve = undefined
            break;
        case 'platform-response':
            const { response: platformResult } = info
            platformResult.worker = typeof document === 'undefined'
            platformResolve && platformResolve(platformResult)
            platformResolve = undefined
            break;
        default:
            break;
    }
}